# def sentiment(message):
#     happy_count = 0
#     sad_count = 0

#     for item in message:
#         if ":-)" in message:
#             happy_count += 1
#         if ":-(" in message:
#             sad_count += 1
#         if happy_count > sad_count:
#             return "happy"
#         if sad_count > happy_count:
#             return "sad"
#         if happy_count == sad_count:
#             return "unsure"
#         if happy_count ==0 and sad_count == 0:
#             return None

# message = ":-) :-("
# print(sentiment(message))

# print (f"emotions:", type(emotions), emotions )
# print (f"happy count:", happy, "||| sad count:", sad)
# print (f"type of happy + sad:", type(happy))

def sentiment(memo):
    emotions = memo.split(" ")
    happy = int(emotions.count(":-)"))
    sad = int(emotions.count(":-("))

    if happy > sad:
        print ("Happy")
    if sad > happy:
        print ("Sad")
    if happy == sad:
        print ("Unsure")
    if happy == 0 and sad == 0:
        print ("None")

memo = ":-) :-( :-) :-("
sentiment(memo)
