from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
admin.site.register(ExpenseCategory)
pass

admin.site.register(Account)
pass

admin.site.register(Receipt)
pass
