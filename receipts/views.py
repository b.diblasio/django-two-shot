from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from receipts.models import Account, ExpenseCategory, Receipt

# Create your views here.
@login_required
def list_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts":receipts,
    }
    return render(request, "list.html", context)
